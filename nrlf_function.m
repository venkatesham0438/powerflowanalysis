% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function nrlf_function(num,ybus,tolerance,linedata,busdata)
BaseMVA = 100;    
bus = busdata(:,1);    
type = busdata(:,2);         
V = busdata(:,3);              
th = busdata(:,4);           
Pg = busdata(:,5);      
Qg = busdata(:,6);
Pl = busdata(:,7);  
Ql = busdata(:,8);   
Qmin = busdata(:,9);     
Qmax = busdata(:,10);
Pi=Pg-Pl;      
Qi=Qg-Ql;
Psp =Pi;            
Qsp=Qi;                 
G = real(ybus);               
B = imag(ybus);                   

pv = find(type == 2 | type == 1);   
pq = find(type == 3);               
npv = length(pv);     % Determining no. of PV buses including slack bus       
npq = length(pq);     % Determining no. of PQ buses

toler = 1;  
Iter = 1;
choice=menu("Do you want to display Values at each iteration?","Yes","No");
switch choice
  case 1
  while (toler > tolerance)
    P = zeros(num,1);
    Q = zeros(num,1);

    for i = 1:num   % Calculating Real and Reactive power at each bus
        for k = 1:num
            P(i) = P(i) + V(i)* V(k)*(G(i,k)*cos(th(i)-th(k)) + B(i,k)*sin(th(i)-th(k)));
            Q(i) = Q(i) + V(i)* V(k)*(G(i,k)*sin(th(i)-th(k)) - B(i,k)*cos(th(i)-th(k)));
        endfor
    endfor

        for n = 2:num    
            if type(n) == 2 % Checking for reactive power limit of PV buses
                QG = Q(n)+Ql(n);
               if (Qmin(n)!=0&&Qmax(n)!=0)
                if QG < Qmin(n)
                    QG=Qmin(n);
                elseif QG > Qmax(n)
                    QG=Qmax(n);
                endif
                endif
            endif
         endfor
    
    dPa = Psp-P;   % Change in real power
    dQa = Qsp-Q;   % Change in reactive power
    k = 1;
    dQ = zeros(npq,1);
    for i = 1:num
        if type(i) == 3
            dQ(k,1) = dQa(i);
            k = k+1;
        endif
    endfor
    dP = dPa(2:num);
    M = [dP; dQ];

    J1 = zeros(num-1,num-1); % Calculating Jacobian matrix J1
    for i = 1:(num-1)
        m = i+1;
        for k = 1:(num-1)
            n = k+1;
            if n == m
                for n = 1:num
                    J1(i,k) = J1(i,k) + V(m)* V(n)*(-G(m,n)*sin(th(m)-th(n)) + B(m,n)*cos(th(m)-th(n)));
                end
                J1(i,k) = J1(i,k) - V(m)^2*B(m,m);
            else
                J1(i,k) = V(m)* V(n)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
            end
        end
    end
    
    J2 = zeros(num-1,npq);    % Calculating Jacobian matrix J2
    for i = 1:(num-1)
        m = i+1;
        for k = 1:npq
            n = pq(k);
            if n == m
                for n = 1:num
                    J2(i,k) = J2(i,k) + V(n)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
                end
                J2(i,k) = J2(i,k) + V(m)*G(m,m);
            else
                J2(i,k) = V(m)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
            end
        end
    end
    
    J3 = zeros(npq,num-1);   % Calcuating Jacobian matrix J3
    for i = 1:npq
        m = pq(i);
        for k = 1:(num-1)
            n = k+1;
            if n == m
                for n = 1:num
                    J3(i,k) = J3(i,k) + V(m)* V(n)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
                end
                J3(i,k) = J3(i,k) - V(m)^2*G(m,m);
            else
                J3(i,k) = V(m)* V(n)*(-G(m,n)*cos(th(m)-th(n)) - B(m,n)*sin(th(m)-th(n)));
            end
        end
    end

    J4 = zeros(npq,npq);   % Calculating Jacobian matrix J4
    for i = 1:npq
        m = pq(i);
        for k = 1:npq
            n = pq(k);
            if n == m
                for n = 1:num
                    J4(i,k) = J4(i,k) + V(n)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
                end
                J4(i,k) = J4(i,k) - V(m)*B(m,m);
            else
                J4(i,k) = V(m)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
            end
        end
    end
    
    J = [J1 J2; J3 J4];  
    
    X = inv(J)*M;    
    dTh = X(1:num-1);  % Calculating change in Voltage Angle
    dV = X(num:end);   % Calculating change in Voltage magnitude
    
    th(2:num) = dTh + th(2:num);
    k = 1;
    for i = 2:num
        if type(i) == 3
            V(i) = dV(k) + V(i);
            k = k+1;
        end
    end
    
    Iter = Iter + 1;   % Incrementing iteration count
    toler = max(abs(M));  % Calculating tolerance
    % Tracing through iteration
    Iter
    V
    th
 end
 case 2
   while (toler > tolerance)
    P = zeros(num,1);
    Q = zeros(num,1);

    for i = 1:num   % Calculating Real and reactive power 
        for k = 1:num
            P(i) = P(i) + V(i)* V(k)*(G(i,k)*cos(th(i)-th(k)) + B(i,k)*sin(th(i)-th(k)));
            Q(i) = Q(i) + V(i)* V(k)*(G(i,k)*sin(th(i)-th(k)) - B(i,k)*cos(th(i)-th(k)));
        endfor
    endfor

        for n = 2:num     % 
            if type(n) == 2
                QG = Q(n)+Ql(n);
                if (Qmin(n)!=0&&Qmax(n)!=0)
                if QG < Qmin(n)
                    QG = Qmin(n);
                elseif QG > Qmax(n)
                    QG=Qmax(n);
                endif
                endif
            endif
         endfor
    
    dPa = Psp-P;  % Calculating change in real power
    dQa = Qsp-Q;  % Calculating change in reactive power
    k = 1;
    dQ = zeros(npq,1);
    for i = 1:num
        if type(i) == 3
            dQ(k,1) = dQa(i);
            k = k+1;
        endif
    endfor
    dP = dPa(2:num);
    M = [dP; dQ];

    J1 = zeros(num-1,num-1);  % Calculating Jacobian matrix J1
    for i = 1:(num-1)
        m = i+1;
        for k = 1:(num-1)
            n = k+1;
            if n == m
                for n = 1:num
                    J1(i,k) = J1(i,k) + V(m)* V(n)*(-G(m,n)*sin(th(m)-th(n)) + B(m,n)*cos(th(m)-th(n)));
                end
                J1(i,k) = J1(i,k) - V(m)^2*B(m,m);
            else
                J1(i,k) = V(m)* V(n)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
            end
        end
    end
    
    J2 = zeros(num-1,npq);  % Calculating Jacobian matrix J2
    for i = 1:(num-1)
        m = i+1;
        for k = 1:npq
            n = pq(k);
            if n == m
                for n = 1:num
                    J2(i,k) = J2(i,k) + V(n)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
                end
                J2(i,k) = J2(i,k) + V(m)*G(m,m);
            else
                J2(i,k) = V(m)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
            end
        end
    end
    
    J3 = zeros(npq,num-1);  % Calculating Jacobian matrix J3
    for i = 1:npq
        m = pq(i);
        for k = 1:(num-1)
            n = k+1;
            if n == m
                for n = 1:num
                    J3(i,k) = J3(i,k) + V(m)* V(n)*(G(m,n)*cos(th(m)-th(n)) + B(m,n)*sin(th(m)-th(n)));
                end
                J3(i,k) = J3(i,k) - V(m)^2*G(m,m);
            else
                J3(i,k) = V(m)* V(n)*(-G(m,n)*cos(th(m)-th(n)) - B(m,n)*sin(th(m)-th(n)));
            end
        end
    end

    J4 = zeros(npq,npq);  % Calculating Jacobian matrix J4
    for i = 1:npq
        m = pq(i);
        for k = 1:npq
            n = pq(k);
            if n == m
                for n = 1:num
                    J4(i,k) = J4(i,k) + V(n)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
                end
                J4(i,k) = J4(i,k) - V(m)*B(m,m);
            else
                J4(i,k) = V(m)*(G(m,n)*sin(th(m)-th(n)) - B(m,n)*cos(th(m)-th(n)));
            end
        end
    end
    
    J = [J1 J2; J3 J4];   % Formation of Jacobian matrix
    
    X = inv(J)*M;    
    dTh = X(1:num-1);    % Change in Voltage Angle
    dV = X(num:end);     % Change in Voltage Magnitude
    
    th(2:num) = dTh + th(2:num);
    k = 1;
    for i = 2:num
        if type(i) == 3
            V(i) = dV(k) + V(i);
            k = k+1;
        end
    end
    
    Iter = Iter + 1;
    toler = max(abs(M));
end
endswitch
loadflow_nrlf(num,V,th,linedata,busdata,ybus);
endfunction