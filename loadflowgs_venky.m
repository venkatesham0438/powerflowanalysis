% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function [Pi Qi Pg Qg Pl Ql] = loadflowgs_venky(num,V,th,linedata,ybus,busdata)
BMva=100;
Vm = pol2rect1(V,th); 
th = (180/pi)*th; 
fb = linedata(:,1); 
tb = linedata(:,2);
b=linedata(:,5);
nl = length(fb);
Pl = busdata(:,7); 
Ql = busdata(:,8);
Iij = zeros(num,num);
Sij = zeros(num,num);
Si = zeros(num,1);

I = ybus*Vm;  % Ybus * Voltage
Im = abs(I);  % Determining absolute value of I
Ia = angle(I);  % Determining angle of I
 
 for m = 1:nl  % Calculating Line currents
     p = fb(m); q = tb(m);
     Iij(p,q) = -(Vm(p) - Vm(q))*ybus(p,q)+b(m)*Vm(p);
     Iij(q,p) = -(Vm(q) - Vm(p))*ybus(q,p)+b(m)*Vm(q);
 end
 Iij = sparse(Iij);
 Iijm = abs(Iij);
 Iija = angle(Iij);

 for m = 1:num  % Calculating Power
     for n = 1:num
         if m ~= n
             Sij(m,n) = Vm(m)*conj(Iij(m,n))*BMva;
         end
     end
 end
 Sij = sparse(Sij);
 Pij = real(Sij);
 Qij = imag(Sij);
 
 Lij = zeros(nl,1);
 for m = 1:nl % Calculating total line losses
     p = fb(m); q = tb(m);
     Lij(m) = Sij(p,q) + Sij(q,p);
 end
 Lpij = real(Lij);
 Lqij = imag(Lij);
 for i = 1:num
     for k = 1:num
         Si(i) = Si(i) + conj(Vm(i))* Vm(k)*ybus(i,k)*BMva;
     end
 end
 Pi = real(Si);
 Qi = -imag(Si);
 Pg = Pi+Pl;
 Qg = Qi+Ql;

disp('#########################################################################################');
disp('-----------------------------------------------------------------------------------------');
disp('                              Gauss Siedel Loadflow Analysis ');
disp('-----------------------------------------------------------------------------------------');
disp('| Bus |    V   |  Angle  |     Injection      |     Generation     |          Load      |');
disp('| No  |   pu   |  Degree |    MW   |   MVar   |    MW   |  Mvar    |     MW     |  MVar | ');
for m = 1:num
    disp('-----------------------------------------------------------------------------------------');
    fprintf('%3g', m); fprintf('  %8.4f', V(m)); fprintf('   %8.4f', th(m));
    fprintf('  %8.3f', Pi(m)); fprintf('   %8.3f', Qi(m)); 
    fprintf('  %8.3f', Pg(m)); fprintf('   %8.3f', Qg(m)); 
    fprintf('  %8.3f', Pl(m)); fprintf('   %8.3f', Ql(m)); fprintf('\n');
end
disp('-----------------------------------------------------------------------------------------');
fprintf(' Total                  ');fprintf('  %8.3f', sum(Pi)); fprintf('   %8.3f', sum(Qi)); 
fprintf('  %8.3f', sum(Pi+Pl)); fprintf('   %8.3f', sum(Qi+Ql));
fprintf('  %8.3f', sum(Pl)); fprintf('   %8.3f', sum(Ql)); fprintf('\n');
disp('-----------------------------------------------------------------------------------------');
disp('#########################################################################################');

disp('-------------------------------------------------------------------------------------');
disp('                              Line FLow and Losses ');
disp('-------------------------------------------------------------------------------------');
disp('|From|To |    P    |    Q     | From| To |    P     |   Q     |      Line Loss      |');
disp('|Bus |Bus|   MW s   |   MVar   | Bus | Bus|    MW    |  MVar   |     MW   |    MVar  |');
for m = 1:nl
    p = fb(m); q = tb(m);
    disp('-------------------------------------------------------------------------------------');
    fprintf('%4g', p); fprintf('%4g', q); fprintf('  %8.3f', Pij(p,q)); fprintf('   %8.3f', Qij(p,q)); 
    fprintf('   %4g', q); fprintf('%4g', p); fprintf('   %8.3f', Pij(q,p)); fprintf('   %8.3f', Qij(q,p));
    fprintf('  %8.3f', Lpij(m)); fprintf('   %8.3f', Lqij(m));
    fprintf('\n');
end
disp('-------------------------------------------------------------------------------------');
fprintf('   Total Loss                                                 ');
fprintf('  %8.3f', sum(Lpij)); fprintf('   %8.3f', sum(Lqij));  fprintf('\n');
disp('-------------------------------------------------------------------------------------');
disp('#####################################################################################');
save -append venky_gs.txt Pg Qg Pl Ql
bus=busdata(:,1);
La=[bus V th Pg Qg Pl Ql];
for i=1:length(fb)
  p=fb(i);
  q=tb(i);
  Ll(i,:)=[p q Pij(p,q) Qij(p,q) q p Pij(q,p) Qij(q,p) Lpij(i) Lqij(i)];
endfor
La;
Ll;
status=xlswrite("Loadflow",La,"loadflow",'A4:ZZ105264');
status=xlswrite("Lineloss",Ll,"Lineloss",'A2:ZZ105264');
endfunction