% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function B=Busdata_unknown(num)
B=zeros(num,10);
e=1;
disp("Enter busdata value");
while(e<=num)
  B(e,1)=input("Enter bus number:");
  B(e,2)=menu("Type of bus?","Slack bus","PV bus","PQ bus");
  B(e,3)=input("Enter bus Voltage:");
  B(e,4)=input("Enter Voltage Angle:");
  B(e,5)=input("Enter Generating real power:");
  B(e,6)=input("Enter Generating reactive power:");
  B(e,7)=input("Enter reactive load in pu:");
  B(e,8)=input("Enter Reactive load in pu:");
  B(e,9)=input("Enter Minimum reactive power limit, 0 for PQ bus:");
  B(e,10)=input("Enter maximum reactive power limit, 0 for PQ bus:");
  e++;
endwhile;
B
%status=xlswrite("venky_12.xlsx",[] ,"Busdata_unknown");
status=xlswrite("venky_12.xlsx",B ,"Busdata_unknown");
endfunction