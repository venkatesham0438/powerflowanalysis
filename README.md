# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Open an Octave software with preinstalled io package
* Enter command on Octave GUI interface " pkg load io"
* Run Powerflow.m file
* Select a problem from drop-down menu for standard examples or other problem to be solved.
* If other option is selected, enter linedata and busdata.
* Loadflow results and linelosses will be stored in excel sheets lineflow.xlsx and loadflow.xlsx . 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact