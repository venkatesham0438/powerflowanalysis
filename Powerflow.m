% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06
clc;
clear;
prob=menu("Select problem to be solved","3-bus","5-bus","14-bus","Stevenson_5_bus","30 bus","33 bus","Other");
switch prob
   case 1
    num=3;
    linedata = xlsread('Linedata_venky.xlsx','Linedata3','A2:F4');
    busdata =xlsread('Linedata_venky.xlsx','Busdata3','A2:J4');
    ybus=Ybus_venky(num,linedata);
   case 2
    num=5;
    linedata=xlsread('Linedata_venky.xlsx','Linedata5','A2:F8');
    busdata =xlsread('Linedata_venky.xlsx','Busdata5','A2:J6');
    ybus=Ybus_venky(num,linedata);
   case 3
    num=14;
    linedata=xlsread('Linedata_venky.xlsx','Linedata14','A2:F21');
    busdata =xlsread('Linedata_venky.xlsx','Busdata14','A2:J15');
    ybus=Ybus_venky(num,linedata);
   case 4
    num=5;
    linedata=xlsread('Linedata_venky.xlsx','Steve_Line','A2:F8');
    busdata =xlsread('Linedata_venky.xlsx','Steve_Bus','A2:J6');
    ybus=Ybus_venky(num,linedata);
    case 5
     num=30;
    linedata=xlsread('Linedata_venky.xlsx','Linedata_30','A2:F42');
    busdata =xlsread('Linedata_venky.xlsx','Busdata_30','A2:J31');
    ybus=Ybus_venky(num,linedata);
    case 6
     num=33;
    linedata=xlsread('Linedata_venky.xlsx','Linedata_30','A2:F42');
    busdata =xlsread('Linedata_venky.xlsx','Busdata_30','A2:J31');
    ybus=Ybus_venky(num,linedata);
   case 7
    [ybus,num,linedata]=Ybus_unknown();
    busdata=Busdata_unknown(num);
endswitch 
tolerance=input("Tolerance:");
meth=menu("Select the method to solve Power Flow problem","Gauss Siedel","Newton Raphson");
switch meth 
  case 1
  Gauss_siedel(num,ybus,tolerance,linedata,busdata);
  case 2
  var1=menu("Do you want to limit the number of iterations","Yes","No");
  switch var1
    case 1
    nrlf_limititerations(num,ybus,tolerance,linedata,busdata);
    case 2
    nrlf_function(num,ybus,tolerance,linedata,busdata);
  endswitch
endswitch