% Function to form Ybus from inspection
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function ybus=Ybus_venky(num,linedata)
fb = linedata(:,1); % Reads from bus   
tb = linedata(:,2); % Reads  to bus
r = linedata(:,3);  % Reads resistance  
x = linedata(:,4);  % Reads  reactance
b = linedata(:,5);  % Reads  Half shunt reactance
a = linedata(:,6);  % Reads  transformer tap setting
z = r + i*x;            
y = 1./z;              
b = i*b;

nbus = max(max(fb),max(tb)); % Determine number of buses
nbranch = length(fb);        % Determine number of elements
ybus = zeros(nbus,nbus);    
 
% Formation of diagonal elements
 for k=1:nbranch
     ybus(fb(k),tb(k)) = ybus(fb(k),tb(k))-y(k)/a(k);
     ybus(tb(k),fb(k)) = ybus(fb(k),tb(k));
 end
 
 % Formation of diagonal elements
 for m =1:nbus
     for n =1:nbranch
         if fb(n) == m
             ybus(m,m) =ybus(m,m) + y(n)/(a(n)^2) + b(n);
         elseif tb(n) == m
             ybus(m,m) = ybus(m,m) + y(n) + b(n);
         end
     end
 end
 ybus;
 zbus=inv(ybus);
 endfunction