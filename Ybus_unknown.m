% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function [Ybus,n,L]=Ybus_unknown()
n=input("Number of buses=");
e=input("Enter no. of elements=");
acap=zeros(e,n+1);
Z=zeros(e,e);
L=zeros(e,9);
k=1;
while(e!=0)
  L(k,1)=k;
  p=input("From node:");
  L(k,2)=p;
  q=input("To node:");
  L(k,3)=q;
  acap(k,p)=1;
  acap(k,q)=-1;
  r=input("Enter the resistance of the element:");
  L(k,4)=r;
  x=input("Enter the reactance of the element:");
  L(k,5)=x;
  Z(k,k)=r+i*x;
  shunt=menu("Is this element has shunt admittance","Yes","No");
  switch shunt
    case 1
    b=input("enter half susceptance value:");
    L(k,9)=b;
    case 2
    L(k,9)=0;
  endswitch
  choice=menu("Is this line mutually coupled","Yes","No");
  switch choice
    case 1
      mu=input("Enter mutually coupled line number:");
      L(k,6)=mu;
      rmu=input("Enter resistance of mutually coupled line:");
      L(k,7)=rmu;
      xmu=input("Enter reactance of mutually coupled line:");
      L(k,8)=xmu;
      Z(k,mu)=rmu+i*xmu;
      Z(mu,k)=rmu+i*xmu;
    case 2
     L(k,6)=0;
     L(k,7)=0;
     L(k,8)=0;
  endswitch
  e=e-1;
  k=k+1;
  endwhile
  a=acap;
  a(:,n+1)=[]
  y=inv(Z);
  Ybus=(a'*y)*a;
  L
   status=xlswrite("venky_newdata.xlsx",L,"Sheet1",'A2:ZZ105264');
  e=n;
  while(e!=0)
    if(L(e,9)!=0)
      Ybus(L(e,2),L(e,2))+=L(e,9);
      Ybus(L(e,3),L(e,3))+=L(e,9);   
    else
    endif
   e=e-1;
  endwhile
 Ybus
 save venky_gs.txt Ybus
endfunction