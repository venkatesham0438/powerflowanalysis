% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

function Gauss_siedel(num,ybus,tolerance,linedata,busdata)
bus = busdata(:,1);  
type = busdata(:,2);
V = busdata(:,3);
th = busdata(:,4);
GenMW = busdata(:,5);
GenMVAR = busdata(:,6);
LoadMW = busdata(:,7);  
LoadMVAR = busdata(:,8);
Qmin = busdata(:,9);
Qmax = busdata(:,10);
nbus = max(bus);  
P = GenMW - LoadMW;     
Q = GenMVAR - LoadMVAR;
Vprev = V;            
alfa=1.6;
choice=menu("Do you want to limit the iteration count?","Yes","No");
switch(choice)
case 1
  maxiteration=input("Enter max. iterations=");
  it=menu("Do you want to display values at each iteration","Yes","No");
   switch(it)
    case 1
    iteration=1;  
    while(iteration<maxiteration)
     for i = 2:nbus
       ibus = 0;
       for k = 1:nbus
         if i ~= k
           ibus = ibus + ybus(i,k)* V(k);
          end
       end
       if type(i) == 2  % Determining Qi for PV buses
         Q(i) = -imag(conj(V(i))*(ibus + ybus(i,i)*V(i)));
       if ((Qmin(i)!=0)&&(Qmax(i)!=0))
         if (Q(i) > Qmax(i)) || (Q(i) < Qmin(i)) % Checking Qi limit
           if Q(i) < Qmin(i)  
              Q(i) = Qmin(i);
           else   
              Q(i) = Qmax(i);
           end
          end
         end
        end
        V(i) = (1/ybus(i,i))*((P(i)-j*Q(i))/conj(V(i)) - ibus); % Compute Bus Voltages
        if type(i) == 2   % Computing Voltage of PV buses
            vc(i)=abs(Vprev(i))*V(i)/abs(V(i));
           V(i)=vc(i);
        end
        if type(i) == 3  % Computing accelerating Voltages for PQ buses
        Vacc(i)= V(i)+alfa*(V(i)-Vprev(i));
        V(i)=Vacc(i);
       end
     end      
    toler = max(abs(abs(V) - abs(Vprev)));
    if(toler<tolerance)  % Checking for tolerance
    break;
    end
    % Tracing through iteration
    iteration
    V
    Vmag=abs(V)
    Ang = 180/pi*angle(V)
    save -append venky.txt iteration V Vmag Ang 
    iteration=iteration+1; %increment iteration count
    Vprev = V;
   end
   case 2
   iteration=1;  
    while(iteration<maxiteration)
     for i = 2:nbus
       ibus = 0;
       for k = 1:nbus
         if i ~= k
           ibus = ibus + ybus(i,k)* V(k); %Yik*Vk
          end
       end
       if type(i) == 2  % Computing Qi for PV buses
         Q(i) = -imag(conj(V(i))*(ibus + ybus(i,i)*V(i)));
         if (Q(i) > Qmax(i)) || (Q(i) < Qmin(i)) 
           if Q(i) < Qmin(i)  
              Q(i) = Qmin(i);
           else   
              Q(i) = Qmax(i);
           end
          end
         end
        V(i) = (1/ybus(i,i))*((P(i)-j*Q(i))/conj(V(i)) - ibus); % Computing bus Volatges
        if type(i) == 2 % Correcting bus voltage for PV buses
              vc(i)=abs(Vprev(i))*V(i)/abs(V(i));
           V(i)=vc(i);
        endif
        if busdata(i,8) == 3  % Calculating accelerating Voltage for PQ buses
        Vacc(i)= Vprev(i)+alfa*(V(i)-Vprev(i));
        V(i)=Vacc(i);
       endif
     end      
    toler = max(abs(abs(V) - abs(Vprev)));
    if(toler<tolerance)  % Checking for tolerance
    break;
    end
    iteration=iteration+1; % Increment iteration count
    Vprev = V;
   end
   end
  case 2
  it=menu("Do you want to display values at each iteration","Yes","No");
  switch(it)
  case 1
  iteration=1;
  toler=1;
  while(toler>tolerance)
    for i = 2:nbus
      ibus = 0;
      for k = 1:nbus
        if i ~= k
          ibus = ibus + ybus(i,k)* V(k);
        end
      end
        if type(i) == 2   % Computing Qi for PV buses
            Q(i) = -imag(conj(V(i))*(ibus + ybus(i,i)*V(i)));
            if (Q(i) > Qmax(i)) || (Q(i) < Qmin(i)) 
                if Q(i) < Qmin(i)  
                    Q(i) = Qmin(i);
                else   
                    Q(i) = Qmax(i);
                end
            end
        end
        V(i) = (1/ybus(i,i))*((P(i)-j*Q(i))/conj(V(i)) - ibus); % Computing bus Volatges
        if type(i) == 2  % Correcting PV bus Volatges
            vc(i)=abs(Vprev(i))*V(i)/abs(V(i));
           V(i)=vc(i);
        end
        if busdata(i,8) == 3  % Calculating accelerating Voltage for PQ buses
        Vacc(i)= Vprev(i)+alfa*(V(i)-Vprev(i));
        V(i)=Vacc(i);
       endif
     end      
    toler = max(abs(abs(V) - abs(Vprev)));  % Calculating tolerance
    % Displaying Values at each iteration
    iteration
    V
    Vmag=abs(V)
    Angle=180/pi*angle(V)
    save -append venky.txt iteration V Vmag Ang   % Saving Values of each iteration to text file. 
    iteration=iteration+1;  % Increment iteration count
    Vprev = V;
   end
  case 2
  iteration=1;
  toler=1;
  while(toler>tolerance)
    for i = 2:nbus
      ibus = 0;
      for k = 1:nbus
        if i ~= k
          ibus = ibus + ybus(i,k)* V(k);
        end
      end
        if type(i) == 2  % Calculating Qi for PV buses
            Q(i) = -imag(conj(V(i))*(ibus + ybus(i,i)*V(i)));
            if (Q(i) > Qmax(i)) || (Q(i) < Qmin(i)) 
                if Q(i) < Qmin(i)  
                    Q(i) = Qmin(i);
                else   
                    Q(i) = Qmax(i);
                end
            end
        end
        V(i) = (1/ybus(i,i))*((P(i)-j*Q(i))/conj(V(i)) - ibus); % Calculating bus Voltages
        if type(i) == 2 % Correcting PV bus Voltages
            vc(i)=abs(Vprev(i))*V(i)/abs(V(i));
           V(i)=vc(i);
        end
        if busdata(i,8) == 3  % Calculating accelerating Voltage for PQ buses
        Vacc(i)= Vprev(i)+alfa*(V(i)-Vprev(i));
        V(i)=Vacc(i);
       endif
   end      
    toler = max(abs(abs(V) - abs(Vprev)));
    iteration=iteration+1;
    Vprev = V;
  end
 end
end
Ang = angle(V);
Vmag=abs(V);
for k=1:num
  vbus(k,1)=V(k,1)*(cos(Ang(k,1))+i*sin(Ang(k,1)));
  k++;
endfor
iteration
save -append venky_gs.txt iteration V Vmag Ang
loadflowgs_venky(num,Vmag,Ang,linedata,ybus,busdata);
endfunction